var express = require('express');
var router = express.Router();
var controller = require('../controller');

router.get('/', controller.home.index);

module.exports = router;
