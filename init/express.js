var express = require('express');
var express_routes = require('./express-routes');
var app = express();

app.set('view engine', 'ejs');
app.set('views', __dirname + '/../views');
app.engine('html', require('ejs').__express);

app.use(express_routes);
app.use(express.static(__dirname + '/../public'));

module.exports = app;
