var config = require('config');
var http = require('http');
var init = require('./init');
var app = init.express;
var server = http.createServer(app);
var port = process.argv[2] || config.get('PORT');

server.listen(port);

console.log('application started on port ' + port);
