// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var jsFramework = [
  "bower_components/angular/angular.min.js",
  "bower_components/angular-animate/angular-animate.min.js",
  "bower_components/angular-aria/angular-aria.min.js",
  "bower_components/angular-aria/angular-messages.min.js",
  "bower_components/angular-material/angular-material.min.js",
  "bower_components/angular-material-icons/angular-material-icons.min.js"
];

var cssFramework = [
  "bower_components/angular-material/angular-material.min.css"
];

gulp.task('js-framework', function() {
    return gulp.src(jsFramework)
    .pipe(concat('frameworks.js'))
    .pipe(gulp.dest('public/js'));
});

gulp.task('css-framework', function(){
  return gulp.src(cssFramework)
  .pipe(concat('frameworks.css'))
  .pipe(gulp.dest('public/css'));
});

// Compile Our Sass
gulp.task('sass-site', function() {
    return gulp.src('sass/*.scss')
        .pipe(sass())
        .pipe(concat('site.css'))
        .pipe(gulp.dest('public/css'));
});

gulp.task('js-site', function() {
    return gulp.src('javascript/*.js')
        .pipe(concat('site.js'))
        .pipe(gulp.dest('public/js'))
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('javascript/*.js', ['dev']);
    gulp.watch('sass/*.scss', ['dev']);
});

// Default Task
//gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);
gulp.task('dev', ['js-framework', 'css-framework', 'sass-site', 'js-site', 'watch']);
